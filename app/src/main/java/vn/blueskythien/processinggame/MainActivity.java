package vn.blueskythien.processinggame;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;

import processing.android.PFragment;
import vn.blueskythien.processinggame.engine.GActivity;
import vn.blueskythien.processinggame.engine.GameApplet;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PFragment fragment = new PFragment();
        GameApplet game = new GameApplet(new GActivity());
        fragment.setSketch(game);
        getFragmentManager().beginTransaction().replace(R.id.drawer_space, fragment).commit();
        SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor accel = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        manager.registerListener(game, accel, SensorManager.SENSOR_DELAY_GAME);
    }
}
