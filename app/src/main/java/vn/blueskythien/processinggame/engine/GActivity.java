package vn.blueskythien.processinggame.engine;

/**
 * Created by DuThien on 20/05/2017.
 */

public class GActivity implements GMouseTracking.GMouseTrackingListener {
    public GameApplet p;

    public final void onAttach(GameApplet p) {
        this.p = p;
    }

    public void onCreated() {

    }

    public void onResume() {

    }

    public void onPause() {

    }

    public void onStop() {

    }


    public final void finish() {
        p.popActivity(this);
    }

    public final void startActivity(GActivity act) {
        p.pushActivity(act);
    }


    public void draw() {

    }

    public void update(float deltaTime) {

    }


    @Override
    public boolean onMousePressed(GMouseTracking tracking, int mouseId) {
        return false;
    }

    @Override
    public boolean onMouseRelease(GMouseTracking tracking, int mouseId) {
        return false;
    }

    @Override
    public void onMouseMove(GMouseTracking tracking, int mouseId) {

    }

    public void onSensorChange(float gx, float gy, float gz) {

    }
}
