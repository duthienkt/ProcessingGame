package vn.blueskythien.processinggame.engine;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import processing.core.PApplet;

/**
 * Created by DuThien on 20/05/2017.
 */

public class GameApplet extends PApplet implements GMouseTracking.GMouseTrackingListener, SensorEventListener {
    /**
     * Stack of activity
     */

    private GActivity[] acts;
    private int nAct;
    private GActivity act;

    public GameApplet(GActivity root) {
        this.act = root;
    }

    @Override
    public void settings() {
        fullScreen(P2D);
    }

    @Override
    public void setup() {
        acts = new GActivity[40];
        nAct = 0;
        act.onAttach(this);
        act.onCreated();
        act.onResume();
    }

    public void pushActivity(GActivity act) {
        this.act.onPause();
        acts[nAct++] = this.act;
        this.act = act;
        act.onAttach(this);
        act.onCreated();
        act.onResume();
    }


    public void popActivity(GActivity act) {
        if (nAct <= 0) {
            //// TODO: 20/05/2017 exit app or do nothing
        } else if (acts[nAct - 1] == act) {
            act.onPause();
            act.onStop();
            act = acts[--nAct];
            act.onResume();
        }
    }

    @Override
    public boolean onMousePressed(GMouseTracking tracking, int mouseId) {
        return act.onMousePressed(tracking, mouseId);
    }

    @Override
    public boolean onMouseRelease(GMouseTracking tracking, int mouseId) {
        return act.onMouseRelease(tracking, mouseId);
    }

    @Override
    public void onMouseMove(GMouseTracking tracking, int mouseId) {
         act.onMouseMove(tracking, mouseId);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        act.onSensorChange(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
